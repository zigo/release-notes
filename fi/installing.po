# Tapio Lehtonen <tale@debian.org>, 2005, 2007, 2009.
# Tommi Vainikainen <thv@iki.fi>, 2006-2007.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes\n"
"POT-Creation-Date: 2011-01-08 18:52+0100\n"
"PO-Revision-Date: 2009-09-16 14:31+0300\n"
"Last-Translator: Tapio Lehtonen <tale@debian.org>\n"
"Language-Team: Finnish <debian-l10n-finnish@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: en/installing.dbk:7
msgid "en"
msgstr "fi"

#: en/installing.dbk:8
msgid "Installation System"
msgstr "Asennusjärjestelmä"

#: en/installing.dbk:10
msgid ""
"The Debian Installer is the official installation system for Debian.  It "
"offers a variety of installation methods.  Which methods are available to "
"install your system depends on your architecture."
msgstr ""
"Debianin asennin on virallinen asennusjärjestelmä Debianille. Asennin "
"tarjoaa useita asennustapoja. Tiettyyn järjestelmään tarjolla olevat tavat "
"riippuvat arkkitehtuurista."

#: en/installing.dbk:15
msgid ""
"Images of the installer for &releasename; can be found together with the "
"Installation Guide on the <ulink url=\"&url-installer;\">Debian website</"
"ulink>."
msgstr ""
"Asentimen otokset julkaisulle &releasename; ja Asennusohje löytyvät <url id="
"\"&url-installer;\" name=\"Debianin seittisivustosta\">."

#: en/installing.dbk:20
msgid ""
"The Installation Guide is also included on the first CD/DVD of the official "
"Debian CD/DVD sets, at:"
msgstr ""
"Asennusohje on myös mukana Debianin romppusarjan ensimmäisellä rompulla "
"tiedostossa"

#: en/installing.dbk:23
#, no-wrap
msgid "/doc/install/manual/<replaceable>language</replaceable>/index.html\n"
msgstr "/doc/install/manual/<replaceable>kieli</replaceable>/index.html\n"

#: en/installing.dbk:27
msgid ""
"You may also want to check the <ulink url=\"&url-installer;index#errata"
"\">errata</ulink> for debian-installer for a list of known issues."
msgstr ""
"Kannattaa tarkistaa myös debianin asentimen <url id=\"&url-installer;"
"index#errata\" name=\"virheluettelosta\"> tunnetut viat."

#: en/installing.dbk:32
msgid "What's new in the installation system?"
msgstr "Mitä uutta asennusjärjestelmässä?"

#: en/installing.dbk:34
msgid ""
"There has been a lot of development on the Debian Installer since its first "
"official release with &debian; 3.1 (sarge)  resulting in both improved "
"hardware support and some exciting new features."
msgstr ""
"Debianin asenninta on kehitetty paljon sitten sen ensimmäisen virallisen "
"julkaisun &debian; 3.1:ssä (sargessa). Laitetuki on parantunut ja uusia "
"jännittäviä ominaisuuksia on lisätty."

#: en/installing.dbk:40
msgid ""
"In these Release Notes we'll only list the major changes in the installer.  "
"If you are interested in an overview of the detailed changes since "
"&oldreleasename;, please check the release announcements for the "
"&releasename; beta and RC releases available from the Debian Installer's "
"<ulink url=\"&url-installer-news;\">news history</ulink>."
msgstr ""
"Tässä julkistusmuistiossa luetellaan vain asentimen suuret muutokset. "
"Katsaus yksityiskohtaisiin muutoksiin &oldreleasename;:n jälkeen löytyy "
"julkaisun &releasename; beta- ja RC-julkaisuista, jotka ovat saatavilla "
"Debianin Asentimen <url id=\"&url-installer-news\" name=\"vanhoista uutisista"
"\">."

#: en/installing.dbk:48
msgid "Major changes"
msgstr "Suuret muutokset"

#: en/installing.dbk:73
msgid "Dropped platforms"
msgstr ""

#: en/installing.dbk:76
msgid ""
"Support for the Alpha ('alpha') and ARM ('arm') architectures has been "
"dropped from the installer, the latter due to it being made obsoleted by the "
"armel port"
msgstr ""

#: en/installing.dbk:84
msgid "Support for kFreeBSD"
msgstr ""

#: en/installing.dbk:87
msgid ""
"The installer can be used to install the kFreeBSD instead of the Linux "
"kernel and test the technology preview. To use this feature the appropriate "
"installation image (or CD/DVD set) has to be used."
msgstr ""

#: en/installing.dbk:95
msgid "New supported platforms"
msgstr ""

#: en/installing.dbk:98
msgid "The installation system now supports the following platforms:"
msgstr ""

#: en/installing.dbk:103
msgid "Intel Storage System SS4000-E"
msgstr ""

#: en/installing.dbk:108
msgid "Marvell's Kirkwood platform:"
msgstr ""

#: en/installing.dbk:114
msgid "QNAP TS-110, TS-119, TS-210, TS-219, TS-219P and TS-419P"
msgstr ""

#: en/installing.dbk:119
msgid "Marvell SheevaPlug and GuruPlug"
msgstr ""

#: en/installing.dbk:124
msgid "Marvell OpenRD-Base, OpenRD-Client and OpenRD-Ultimate"
msgstr ""

#: en/installing.dbk:132
msgid "HP t5325 Thin Client (partial support)"
msgstr ""

#: en/installing.dbk:142
msgid "GRUB 2 is the default bootloader"
msgstr ""

#: en/installing.dbk:145
msgid ""
"The bootloader that will be installed by default is <systemitem role="
"\"package\">grub-pc</systemitem> (GRUB 2)."
msgstr ""

#: en/installing.dbk:152
#, fuzzy
#| msgid "What's new in the installation system?"
msgid "Help during the installation process"
msgstr "Mitä uutta asennusjärjestelmässä?"

#: en/installing.dbk:155
msgid ""
"The dialogs presented during the installation process now provide help "
"information. Although not currently used in all dialogs, this feature would "
"be increasingly used in future releases. This will improve the user "
"experience during the installation process, especially for new users."
msgstr ""

#: en/installing.dbk:164
msgid "Installation of Recommended packages"
msgstr ""

#: en/installing.dbk:167
msgid ""
"The installation system will install all recommended packages by default "
"throughout the process except for some specific situations in which the "
"general setting gives undesired results."
msgstr ""

#: en/installing.dbk:175
msgid "Automatic installation of hardware-specific packages"
msgstr ""

#: en/installing.dbk:178
msgid ""
"The system will automatically select for installation hardware-specific "
"packages when they are appropriate. This is achieved through the use of "
"<literal>discover-pkginstall</literal> from the <systemitem role=\"package"
"\">discover</systemitem> package."
msgstr ""

#: en/installing.dbk:187
msgid "Support for installation of previous releases"
msgstr ""

#: en/installing.dbk:190
msgid ""
"The installation system can be also used for the installation of previous "
"release, such as &oldreleasename;."
msgstr ""

#: en/installing.dbk:197
msgid "Improved mirror selection"
msgstr ""

#: en/installing.dbk:200
msgid ""
"The installation system provides better support for installing both "
"&releasename; as well as &oldreleasename; and older releases (through the "
"use of archive.debian.org). In addition, it will also check that the "
"selected mirror is consistent and holds the selected release."
msgstr ""

#: en/installing.dbk:210
msgid "Changes in partitioning features"
msgstr ""

#: en/installing.dbk:213
msgid ""
"This release of the installer supports the use of the ext4 file system and "
"it also simplifies the creation of RAID, LVM and crypto protected "
"partitioning systems. Support for the reiserfs file system is no longer "
"included by default, although it can be optionally loaded."
msgstr ""

#: en/installing.dbk:223
#, fuzzy
#| msgid "Support for loading firmware during installation"
msgid "Support for loading firmware debs during installation"
msgstr "Tuetaan laiteohjelmiston lataamista asennuksen aikana"

#: en/installing.dbk:226
msgid ""
"It is now possible to load firmware package files from the installation "
"media in addition to removable media, allowing the creation of PXE images "
"and CDs/DVDs with included firmware packages."
msgstr ""

#: en/installing.dbk:232
msgid ""
"Starting with Debian &release;, non-free firmware has been moved out of "
"main.  To install Debian on hardware that needs non-free firmware, you can "
"either provide the firmware yourself during installation or use pre-made non-"
"free CDs/DVDs which include the firmware. See the <ulink url=\"http://www."
"debian.org/distrib\">Getting Debian section</ulink> on the Debian website "
"for more information."
msgstr ""

#: en/installing.dbk:243
msgid "New languages"
msgstr "Uudet kielet"

#: en/installing.dbk:246
msgid ""
"Thanks to the huge efforts of translators, &debian; can now be installed in "
"67 languages.  This is three more languages than in &oldreleasename;.  Most "
"languages are available in both the text-based installation user interface "
"and the graphical user interface, while some are only available in the "
"graphical user interface."
msgstr ""

#: en/installing.dbk:254
msgid "Languages added in this release include:"
msgstr ""

#: en/installing.dbk:260
msgid ""
"Asturian, Estonian, Kazakh and Persian have been added to the graphical and "
"text-based installer."
msgstr ""

#: en/installing.dbk:266
msgid "Kannada and Telugu have been added to the graphical installer."
msgstr ""

#: en/installing.dbk:271
msgid ""
"Thai, previously available only in the graphical user interface, is now "
"available also in the text-based installation user interface too."
msgstr ""

#: en/installing.dbk:279
msgid ""
"Due to the lack of translation updates two languages were dropped in this "
"release: Wolof and Welsh."
msgstr ""

#: en/installing.dbk:284
#, fuzzy
#| msgid ""
#| "The languages that can only be selected using the graphical installer as "
#| "their character sets cannot be presented in a non-graphical environment "
#| "are: Amharic, Bengali, Dzongkha, Gujarati, Hindi, Georgian, Khmer, "
#| "Malayalam, Marathi, Nepali, Punjabi, Tamil, and Thai."
msgid ""
"The languages that can only be selected using the graphical installer as "
"their character sets cannot be presented in a non-graphical environment are: "
"Amharic, Bengali, Dzongkha, Gujarati, Hindi, Georgian, Kannada, Khmer, "
"Malayalam, Marathi, Nepali, Punjabi, Tamil and Telugu."
msgstr ""
"Kielet jotka voidaan valita vain graafista asenninta käytettäessä koska "
"niiden merkistöä ei voi näyttää ei-graafisessa ympäristössä ovat: amhara, "
"bengali, bhutani, gudžarati, hindi, gruusia, khmer, malajalam, marathi, "
"nepali, pandžabi, tamili ja thai."

#: en/installing.dbk:293
msgid "Improved localisation selection"
msgstr ""

#: en/installing.dbk:296
msgid ""
"The selection of localisation-related values (language, location and locale "
"settings) is now less interdependent and more flexible. Users will be able "
"to customize the system to their localisation needs more easily while still "
"make it comfortable to use for users that want to select the locale most "
"common for the country they reside in."
msgstr ""

#: en/installing.dbk:303
msgid ""
"Additionally, the consequences of localisation choices (such as timezone, "
"keymap and mirror selection) are now more obvious to the user."
msgstr ""

#: en/installing.dbk:312
msgid "Automated installation"
msgstr "Asennuksen automatisointi"

#: en/installing.dbk:314
msgid ""
"Some changes mentioned in the previous section also imply changes in the "
"support in the installer for automated installation using preconfiguration "
"files.  This means that if you have existing preconfiguration files that "
"worked with the &oldreleasename; installer, you cannot expect these to work "
"with the new installer without modification."
msgstr ""
"Jotkin edellisessä luvussa mainituista muutoksista tarkoittavat muutoksia "
"myös asentimen tuessa asennuksen automatisoinnille valmiita vastauksia "
"käyttämällä. Tämä tarkoittaa, että &oldreleasename;:n asentimen kanssa "
"toimineen valmiiden vastausten tiedoston ei voi olettaa muutoksitta toimivan "
"uuden asentimen kanssa."

#: en/installing.dbk:321
msgid ""
"The <ulink url=\"&url-install-manual;\">Installation Guide</ulink> has an "
"updated separate appendix with extensive documentation on using "
"preconfiguration."
msgstr ""
"<url id=\"&url-install-manual;\" name=\"Asennusohjeessa\"> on nyt erillinen "
"päivitetty liite jossa valmiiden vastausten käyttö kuvataan seikkaperäisesti."

#~ msgid ""
#~ "It is now possible to load firmware binary files from removable media "
#~ "when they're provided externally to Debian installation media."
#~ msgstr ""
#~ "Konekielisiä laiteohjelmistotiedostoja joita ei ole Debianin "
#~ "asennustaltiolla on nyt mahdollista ladata irrotettavalta taltiolta."

#~ msgid ""
#~ "Support for installation from Microsoft Windows "
#~ "<indexterm><primary>Microsoft Windows</primary></indexterm>"
#~ msgstr ""
#~ "Tuki asennukselle Microsoft Windowsista <indexterm><primary>Microsoft "
#~ "Windows</primary></indexterm>"

#~ msgid ""
#~ "The installation media are now provided with an application that allows "
#~ "preparing the system to install Debian from Microsoft Windows "
#~ "environments."
#~ msgstr ""
#~ "Asennustaltiolla on nyt sovellus joka valmistelee järjestelmän siten, "
#~ "että Debianin voi asentaa Microsoft Windowsista."

#~ msgid "<acronym>SATA</acronym> <acronym>RAID</acronym> support"
#~ msgstr "<acronym>SATA</acronym> <acronym>RAID</acronym> -tuki"

#~ msgid "Early upgrade of packages with security fixes"
#~ msgstr "Tietoturvavikojen pikapäivitys"

#~ msgid ""
#~ "When used with functional network access, the installer will upgrade all "
#~ "packages that have been updated since the initial release of "
#~ "&releasename;. This upgrade happens during the installation step, before "
#~ "the installed system is booted."
#~ msgstr ""
#~ "Jos nettiyhteys toimii, päivittää asennin kaikki paketit joista on tullut "
#~ "uusi versio julkaisun &releasename; alkuperäisin julkaisun jälkeen. Tämä "
#~ "päivitys tehdään asennusvaiheessa, ennen kuin asennettu järjestelmä "
#~ "käynnistetään. "

#~ msgid ""
#~ "As a consequence, the installed system is less likely to be vulnerable to "
#~ "security issues that were discovered and fixed between the release time "
#~ "of &releasename; and the installation time."
#~ msgstr ""
#~ "Tämän ansiosta asennettu järjestelmä on vähemmän haavoittuva "
#~ "&releasename;:n julkaisun ja asennushetken välillä löytyneille ja "
#~ "korjatuille tietoturvavioille. "

#~ msgid "Support for <emphasis>volatile</emphasis>"
#~ msgstr "<emphasis>volatile</emphasis>-tuki"

#~ msgid ""
#~ "The installer can now optionally set up the installed system to use "
#~ "updated packages from <literal>volatile.debian.org</literal>. This "
#~ "archive hosts packages providing data that needs to be regularly updated "
#~ "over time, such as timezones definitions, anti-virus signature files, etc."
#~ msgstr ""
#~ "Haluttaessa asennettu järjestelmä käyttää päivitettyjä paketteja "
#~ "julkaisusta <literal>volatile.debian.org</literal>, asennin tekee "
#~ "tarvittavat asetukset. Tässä pakettivarastossa on paketteja jotka on "
#~ "päivitettävä säännöllisin väliajoin, kuten aikavyöhykemääritykset, "
#~ "virustorjunnan tunnistetiedot, jne."

#~ msgid "New boot menu for Intel x86 and AMD64"
#~ msgstr "Uusi käynnistysvalikko Intel x86 ja AMD64 -laitealustoille"

#~ msgid ""
#~ "An interactive boot menu was added to make the choice of specific options "
#~ "and boot methods more intuitive for users."
#~ msgstr ""
#~ "Vuorovaikutteisen valikon ansiosta erikoisvalitsimien ja "
#~ "käynnistystapojen valinta on havainnollisempaa."

#~ msgid "New ports"
#~ msgstr "Uusia siirroksia"

#~ msgid ""
#~ "The armel architecture is now supported. Images for i386 Xen guests are "
#~ "also provided."
#~ msgstr ""
#~ "Laitealusta armel on nyt tuettu. Saatavilla on otokset myös i386 Xen-"
#~ "vieraille."

#~ msgid "Support for hardware speech synthesis devices"
#~ msgstr "Tuki puhesyntetisaattorilaitteille"

#~ msgid ""
#~ "Several devices designed to provide hardware speech synthesis are now "
#~ "supported by the installer, therefore improving its accessibility for "
#~ "visually-impaired users.  <indexterm><primary>visually-impaired users</"
#~ "primary></indexterm>"
#~ msgstr ""
#~ "Asennin tukee nyt useita puhesyntetisaattorilaitteita, parantaen näin "
#~ "esteettömyyttä näkövammaisille. <indexterm><primary>näkövammaiset</"
#~ "primary></indexterm>"

#~ msgid "Support for <literal>relatime</literal> mount options"
#~ msgstr "Tuki liitosvalitsimelle <literal>relatime</literal> "

#~ msgid ""
#~ "The installer can now set up partitions with the <literal>relatime</"
#~ "literal> mount option, so that access time on files and directories is "
#~ "updated only if the previous access time was earlier than the current "
#~ "modify or change time."
#~ msgstr ""
#~ "Asennin osaa nyt tehdä tiedostojärjestelmät käyttämään valitsinta "
#~ "<literal>relatime</literal>, jolloin tiedostojen ja hakemistojen "
#~ "lukemisaikaleimaa päivitetään vain jos se oli aikaisempi kuin sisällön "
#~ "tai hakemistotietueen muutosaikaleima."

#~ msgid "NTP clock synchronization at installation time"
#~ msgstr "Kello aikaan NTP:llä asennusvaiheessa"

#~ msgid ""
#~ "The computer clock is now synchronized with NTP servers over the network "
#~ "during installation so that the installed system immediately has an "
#~ "accurate clock."
#~ msgstr ""
#~ "Tietokoneen kello asetetaan nyt verkon NTP-palvelimilta saatuun oikeaan "
#~ "aikaan asennusvaiheessa, jolloin asennetussa järjestelmässä on heti "
#~ "tarkka kellonaika."

#~| msgid ""
#~| "Thanks to the huge efforts of translators, Debian can now be installed "
#~| "in 47 languages using the text-based installation user interface. This "
#~| "is six languages more than in &oldreleasename;. Languages added in this "
#~| "release include Belarusian, Esperanto, Estonian, Kurdish, Macedonian, "
#~| "Tagalog, Vietnamese and Wolof. Due to lack of translation updates, two "
#~| "languages have been dropped in this release: Persian and Welsh."
#~ msgid ""
#~ "Thanks to the huge efforts of translators, Debian can now be installed in "
#~ "63 languages (50 using the text-based installation user interface and 13 "
#~ "supported only with the graphical user interface).  This is five "
#~ "languages more than in &oldreleasename;.  Languages added in this release "
#~ "include Amharic, Marathi, Irish, Northern Sami, and Serbian.  Due to lack "
#~ "of translation updates, one language has been dropped in this release: "
#~ "Estonian. Another language that was disabled in &oldreleasename; has been "
#~ "reactivated: Welsh."
#~ msgstr ""
#~ "Kiitos kääntäjien jättimäisen työpanoksen voidaan Debian nyt asentaa 63 "
#~ "kielellä (50 kielellä tekstikäyttöliittymää käyttävällä asentimella ja 13 "
#~ "kieltä jotka on tuettu vain graafisessa asentimessa). Tämä on viisi "
#~ "kieltä enemmän kuin julkaisussa &oldreleasename;. Tässä julkaisussa "
#~ "lisättyihin kieliin kuuluvat amhara, marathi, iiri, pohjoissaame ja "
#~ "serbia. Yksi kieli on jätetty tästä julkaisusta pois koska käännöstä ei "
#~ "ole saatettu ajan tasalle: viro. Julkaisussa &oldreleasename; pois "
#~ "käytöstä ollut kieli on otettu takaisin: kymri "

#~ msgid "Simplified country choice"
#~ msgstr "Maan valinta yksinkertaisempi"

#~ msgid ""
#~ "The country choice list is now grouped by continents, allowing an easier "
#~ "selection of country, when users don't want to pick the ones associated "
#~ "with the chosen language."
#~ msgstr ""
#~ "Maaluettelo on nyt ryhmitelty mantereiden mukaan, jolloin maan valinta on "
#~ "helpompaa kun ei haluta valita kielen mukaan määräytyvää maata."
