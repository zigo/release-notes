# Belarusian translation of the Debian Release Notes
# Copyright (C) 2009 Hleb Rubanau
# This file is distributed under the same license as the Debian Release Notes.
# Hleb Rubanau <g.rubanau@gmail.com>, 2009
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2021-03-27 22:34+0100\n"
"PO-Revision-Date: 2009-08-16 00:45+0200\n"
"Last-Translator: Hleb Rubanau <g.rubanau@gmail.com>\n"
"Language-Team: Belarusian Debian <debian-l10n-belarusian@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8-bit\n"

#. type: Attribute 'lang' of: <appendix>
#: en/old-stuff.dbk:8
msgid "en"
msgstr "be"

#. type: Content of: <appendix><title>
#: en/old-stuff.dbk:9
#, fuzzy
#| msgid "Managing your &oldreleasename; system"
msgid "Managing your &oldreleasename; system before the upgrade"
msgstr "Кіраванне сістэмай, заснаванай на &oldreleasename;"

#. type: Content of: <appendix><para>
#: en/old-stuff.dbk:11
msgid ""
"This appendix contains information on how to make sure you can install or "
"upgrade &oldreleasename; packages before you upgrade to &releasename;.  This "
"should only be necessary in specific situations."
msgstr ""
"Гэты дадатак утрымлівае інструкцыі, як перад абнаўленнем да &releasename; "
"пераканацца, што Вы можаце ўсталёўваць альбо абнаўляць пакеты "
"&oldreleasename;. Гэтыя парады патрэбныя толькі ў адмысловых выпадках."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:16
msgid "Upgrading your &oldreleasename; system"
msgstr "Абнаўленне сістэмы на базе &oldreleasename;"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:18
#, fuzzy
#| msgid ""
#| "Basically this is no different than any other upgrade of &oldreleasename; "
#| "you've been doing.  The only difference is that you first need to make "
#| "sure your package list still contains references to &oldreleasename; as "
#| "explained in <xref linkend=\"old-sources\"/>."
msgid ""
"Basically this is no different from any other upgrade of &oldreleasename; "
"you've been doing.  The only difference is that you first need to make sure "
"your package list still contains references to &oldreleasename; as explained "
"in <xref linkend=\"old-sources\"/>."
msgstr ""
"Збольшага, абнаўленне не адрозніваецца ад іншых абнаўленняў, якія рабіліся з "
"сістэмай на базе &oldreleasename;. Адзінае адрозненне ў тым, што найперш Вам "
"варта пераканацца (паводле парадаў з <xref linkend=\"old-sources\"/>), што "
"спіс пакетаў дагэтуль утрымлівае спасыдкі на &oldreleasename;"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:24
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically be "
"upgraded to the latest &oldreleasename; point release."
msgstr ""
"Калі сістэма абнаўляецца праз люстэрка Debian, яно будзе аўтаматычна "
"абноўленае да апошняй рэвізіі &oldreleasename;. "

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:30
#, fuzzy
#| msgid "Checking your sources list"
msgid "Checking your APT source-list files"
msgstr "Праверка спісу крыніц абнаўлення"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:32
#, fuzzy
#| msgid ""
#| "If any of the lines in your <filename>/etc/apt/sources.list</filename> "
#| "refer to 'stable', you are effectively already <quote>using</quote> "
#| "&releasename;.  If you have already run <literal>apt-get update</"
#| "literal>, you can still get back without problems following the procedure "
#| "below."
msgid ""
"If any of the lines in your APT source-list files (see <ulink url=\"&url-"
"man;/&releasename;/apt/sources.list.5.html\">sources.list(5)</ulink>)  "
"contain references to <quote><literal>stable</literal></quote>, this is "
"effectively pointing to &releasename; already. This might not be what you "
"want if you are not yet ready for the upgrade.  If you have already run "
"<command>apt update</command>, you can still get back without problems by "
"following the procedure below."
msgstr ""
"Калі хаця б адзін радок у файле наладак <filename>/etc/apt/sources.list</"
"filename> датычыцца 'stable', то Вы ўжо ў поўнай меры "
"<quote>выкарыстоўваеце</quote> &releasename;. Калі Вы ўжо запускалі "
"<literal>apt-get update</literal>, Вы дагэтуль можаце без праблем адмяніць "
"выкарыстанне &releasename;, ужыўшы апісаную далей працэдуру. "

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:40
msgid ""
"If you have also already installed packages from &releasename;, there "
"probably is not much point in installing packages from &oldreleasename; "
"anymore.  In that case you will have to decide for yourself whether you want "
"to continue or not.  It is possible to downgrade packages, but that is not "
"covered here."
msgstr ""
"Калі Вы ўжо ўсталявалі пакеты з &releasename;, магчыма, больш няма сэнсу "
"далей карыстацца пакетамі з &oldreleasename;. У гэтым выпадку Вам трэба "
"вырашыць, ці варта працягваць працэс адкату. Зніжэнне версіі пакетаў "
"магчымае, але яно не апісваецца ў гэтым дакуменце. "

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:46
#, fuzzy
#| msgid ""
#| "Open the file <filename>/etc/apt/sources.list</filename> with your "
#| "favorite editor (as <literal>root</literal>) and check all lines "
#| "beginning with <literal>deb http:</literal> or <literal>deb ftp:</"
#| "literal> for a reference to <quote><literal>stable</literal></quote>.  If "
#| "you find any, change <literal>stable</literal> to "
#| "<literal>&oldreleasename;</literal>."
msgid ""
"As root, open the relevant APT source-list file (such as <filename>/etc/apt/"
"sources.list</filename>) with your favorite editor, and check all lines "
"beginning with <literal>deb http:</literal>, <literal>deb https:</literal>, "
"<literal>deb tor+http:</literal>, <literal>deb tor+https:</literal>, "
"<literal>URIs: http:</literal>, <literal>URIs: https:</literal>, "
"<literal>URIs: tor+http:</literal> or <literal>URIs: tor+https:</literal> "
"for a reference to <quote><literal>stable</literal></quote>.  If you find "
"any, change <literal>stable</literal> to <literal>&oldreleasename;</literal>."
msgstr ""
"Ад імя карыстальніка <literal>root</literal> з дапамогай свайго ўлюбёнага "
"рэдактара тэксту адчыніце файл <filename>/etc/apt/sources.list</filename> і "
"знайдзіце ўсе радкі, якія пачынаюцца з <literal>deb http:</literal> або "
"<literal>deb ftp:</literal> ды спасылаюцца на <quote><literal>stable</"
"literal></quote>. У такіх радках трэба замяніць <literal>stable</literal> на "
"<literal>&oldreleasename;</literal>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:57
#, fuzzy
#| msgid ""
#| "If you have any lines starting with <literal>deb file:</literal>, you "
#| "will have to check for yourself if the location they refer to contains an "
#| "&oldreleasename; or a &releasename; archive."
msgid ""
"If you have any lines starting with <literal>deb file:</literal> or "
"<literal>URIs: file:</literal>, you will have to check for yourself if the "
"location they refer to contains a &oldreleasename; or &releasename; archive."
msgstr ""
"Калі Вы знойдзеце радкі, што пачынаюцца з <literal>deb file:</literal>, "
"трэба самастойна высветліць, які архіў знаходзіцца ў адпаведнай дырэкторыі, "
"&oldreleasename; або &releasename;."

#. type: Content of: <appendix><section><important><para>
#: en/old-stuff.dbk:64
#, fuzzy
#| msgid ""
#| "Do not change any lines that begin with <literal>deb cdrom:</literal>.  "
#| "Doing so would invalidate the line and you would have to run <command>apt-"
#| "cdrom</command> again.  Do not be alarmed if a 'cdrom' source line refers "
#| "to <quote><literal>unstable</literal></quote>.  Although confusing, this "
#| "is normal."
msgid ""
"Do not change any lines that begin with <literal>deb cdrom:</literal> or "
"<literal>URIs: cdrom:</literal>.  Doing so would invalidate the line and you "
"would have to run <command>apt-cdrom</command> again.  Do not be alarmed if "
"a <literal>cdrom:</literal> source line refers to <quote><literal>unstable</"
"literal></quote>.  Although confusing, this is normal."
msgstr ""
"Не змяняйце радкоў, якія пачынаюцца з <literal>deb cdrom:</literal>. "
"Рэдагаванне зробіць радок непрыдатным, і каб узнавіць яго, давядзецца ізноў "
"запускаць каманду <command>apt-cdrom</command>. Не хвалюйцеся, калі cdrom-"
"крыніца пазначаецца як <quote><literal>unstable</literal></quote>. Гэта "
"выглядае дзіўна, але з'яўляецца нармальным."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:73
msgid "If you've made any changes, save the file and execute"
msgstr "Пасля ўнясення зменаў захавайце файл і запусціце каманду"

#. type: Content of: <appendix><section><screen>
#: en/old-stuff.dbk:76
#, fuzzy, no-wrap
#| msgid "# apt-get update\n"
msgid "# apt update\n"
msgstr "# apt-get update\n"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:79
msgid "to refresh the package list."
msgstr "каб узнавіць спіс пакетаў."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:84
msgid "Removing obsolete configuration files"
msgstr ""

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:86
msgid ""
"Before upgrading your system to &releasename;, it is recommended to remove "
"old configuration files (such as <filename>*.dpkg-{new,old}</filename> files "
"under <filename>/etc</filename>) from the system."
msgstr ""
